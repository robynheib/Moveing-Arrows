Arrow adave;
Arrow bdave;
Arrow cdave;
Arrow ddave;
Arrow edave;
Arrow fdave;
Arrow gdave;
Arrow hdave;

void setup()
{
  size(1000,900);
  adave = new Arrow(10,10,3,6,false,true);
  bdave = new Arrow(50,10,4,8,false,true);
}

void draw()
{
  background(125);
  adave.Update();
  bdave.Update();
  
  if (adave.crash(bdave))
  {
    adave.NSdirection = !adave.NSdirection;
    adave.EWdirection = !adave.EWdirection;
    adave.speedx= adave.speedx*(-1);
    adave.speedy= adave.speedy*(-1);
    bdave.EWdirection = !bdave.EWdirection;
    bdave.NSdirection = !bdave.NSdirection;
    bdave.speedx= bdave.speedx*(-1);
    bdave.speedy= bdave.speedy*(-1);
  }
}