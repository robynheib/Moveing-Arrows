class Arrow
{
  float y;
  float x=31;
  float speedx;
  float speedy;
  boolean NSdirection;
  boolean EWdirection;
  PImage img1;
  PImage img2;
  PImage img3;
  PImage img4;
  
 Arrow( float x, float y, float speedx, float speedy, boolean NSdirection, boolean EWdirection)
 {
   this.x = x;
   this.y = y;
   this.speedx = speedx;
   this.speedy = speedy;
   this.NSdirection = NSdirection;
   this.EWdirection = EWdirection;
   img1=loadImage("arrowNW.png");
   img2=loadImage("arrowNE.png");
   img3=loadImage("arrowSE.png");
   img4=loadImage("arrowSW.png");
 }
 
 void Render()
 {
   if(NSdirection == true && EWdirection == false)
   {
     image(img1, x, y);
   }
   else if(NSdirection == true && EWdirection == true)
   {
     image(img2, x, y);
   }
   else if(NSdirection == false && EWdirection == true)
   {
     image(img3, x, y);
   }
   else
   {
     image(img4, x, y);
   }
 }
 
 void Move()
 {
   x = x + speedx;
   y = y + speedy;
   if (x < 0 || x> (width - 50))
   {
     speedx= speedx*(-1);
     EWdirection = !EWdirection;
   }
   
   if (y < 0 || y> (height - 50))
   {
     speedy= speedy*(-1);
     NSdirection = !NSdirection;
   }
 }
 
 void Update()
 {
   Render();
   Move();
 }
 
 boolean crash(Arrow other)
 {
   return(abs(this.x-other.x) < 20  &&  abs(this.y-other.y) < 20);
 }
}